
angular
.module('app',['ngRoute', 'ngResource', 'ngLoadingSpinner'])
.controller('GitHubController', GitHubController)
.service('GitHubService', GitHubService);

GitHubController.$inject = ['$scope', 'GitHubService'];
GitHubService.$inject = ['$http'];

function GitHubController($scope, GitHubService) {
	
	$scope.getCommits = function(repositoryId) {
		GitHubService.getCommits(repositoryId, handleSuccess, handleError);
		
		function handleSuccess(response) {
			$scope.commits = response.data;
		}

		function handleError(error) {
			return { success: false, message: error };
		}		
	}
	
	$scope.getRepositories = function() {
		GitHubService.getRepositories(handleSuccess, handleError);
		
		function handleSuccess(response) {
			angular.forEach(response.data,function(value,index){
				if(value.full_name === "angular/angular.js") {
					$scope.repositoryId = value.id;
					$scope.getCommits(value.id);
				}				
			})
		}

		function handleError(error) {
			return { success: false, message: error };
		}		
	}
	
	$scope.refresh = function(repositoryId) {
		$scope.getCommits(repositoryId);
	}

	$scope.checkIfNumber = function(hash) {
		var alphabet = hash.slice(-1);
		return /^[0-9]+$/.test(alphabet);
	}
	
	$scope.timediff = function(commitDate){
		commitDate = moment(commitDate);
		var minutes = moment.utc(moment().diff(moment(commitDate))).format("mm");
		// var minutes = moment.utc(moment(new Date()).diff(moment(new Date(commitDate))).format("mm");
		var hours = Math.floor(minutes / 60);
		if(hours > 0) {
			return hours + " hours";
		}
		return minutes + " minutes";
	}
	
	$scope.getRepositories();
}

function GitHubService($http) {
    var service = {
        getCommits: getCommits,
		getRepositories: getRepositories
    };

    return service;	
	
	function getCommits(repoId, handleSuccess, handleError) {
		return $http.get("https://api.github.com/repositories/" + repoId + "/commits").then(handleSuccess, handleError);	  
	}
	
	function getRepositories(handleSuccess, handleError) {
		return $http.get("https://api.github.com/users/angular/repos").then(handleSuccess, handleError);
	}
}